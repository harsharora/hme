import {
    GO_TO_NEXT_WEEK,
    GO_TO_PREVIOUS_WEEK,
    GO_TO_TODAY,
    ADD_OR_UPDATE_EVENT,
    DELETE_EVENT
} from '../actions/constants';
import { getWeekDates } from '../utils/helper';

const initalDate = new Date(),
    initalWeekDates = getWeekDates(initalDate),
    weekStartDate = initalWeekDates.dates[0];

const INITIAL_STATE = {
    today: initalDate,
    dates: initalWeekDates,
    availableEvents: localStorage.getItem('savedEvents') ? JSON.parse(localStorage.getItem('savedEvents')) : {},
    weekStartDate: weekStartDate,
    hoursInADay: 24
}

// Function to get week dates and week start date
const getDates = (startDate, operator) => {
    let weekStartDate = new Date(startDate.valueOf()), // To get copy of date object
        weekStartDateFormatted = new Date(weekStartDate.setDate(weekStartDate.getDate() + operator)),
        dates = getWeekDates(weekStartDateFormatted);
    return {
        weekStartDate: weekStartDateFormatted,
        dates
    }
}

// Storing events local storage
const setLocalStorage = (availableEvents) => localStorage.setItem('savedEvents', JSON.stringify(availableEvents));

// Function to add or update events
const addOrUpdateEvent = (availableEvents, eventData) => {
    if (typeof eventData.eventindex !== 'undefined') {
        availableEvents[eventData.selecteddate][eventData.eventindex] = eventData
    } else {
        if (availableEvents[eventData.selecteddate]) {
            availableEvents[eventData.selecteddate].push(eventData)
        } else {
            availableEvents[eventData.selecteddate] = [eventData]
        }
    }
    setLocalStorage(availableEvents);
    return availableEvents;
}

// Function to delete events
const deleteEvent = (availableEvents, eventData) => {
    if (typeof eventData.eventindex !== 'undefined') {
        availableEvents[eventData.selecteddate] = availableEvents[eventData.selecteddate].filter((_, i) => i != eventData.eventindex)
    }
    setLocalStorage(availableEvents);
    return availableEvents;
}


export default function calendar(state = INITIAL_STATE, action) {
    switch (action.type) {
        case GO_TO_NEXT_WEEK:
            return {
                ...state,
                ...getDates(state.weekStartDate, 7)
            };
        case GO_TO_PREVIOUS_WEEK:
            return {
                ...state,
                ...getDates(state.weekStartDate, -7)
            };
        case GO_TO_TODAY:
             return {
                ...INITIAL_STATE,
             };
        case ADD_OR_UPDATE_EVENT:
             return {
                 ...state,
                 availableEvents: addOrUpdateEvent(state.availableEvents, action.payload)
             }
        case DELETE_EVENT:
             return {
                 ...state,
                 availableEvents: deleteEvent(state.availableEvents, action.payload)
             }
        default:
            return state;
    }
}