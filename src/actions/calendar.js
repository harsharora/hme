import { GO_TO_NEXT_WEEK,
    GO_TO_PREVIOUS_WEEK,
    GO_TO_TODAY,
    ADD_OR_UPDATE_EVENT,
    DELETE_EVENT} from './constants';

export const goToNextWeek = () => {
    return {
        type: GO_TO_NEXT_WEEK
    }
}

export const goToPrevWeek = () => {
    return {
        type: GO_TO_PREVIOUS_WEEK
    }
}

export const goToToday = () => {
    return {
        type: GO_TO_TODAY
    }
}

export const addOrUpdateEvent = (eventObj) => {
    return {
        type: ADD_OR_UPDATE_EVENT,
        payload: eventObj
    }
}

export const deleteEvent = (eventObj) => {
    return {
        type: DELETE_EVENT,
        payload: eventObj
    }
}