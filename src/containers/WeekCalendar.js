import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getTimeIn24HoursFormat,
    getMinutesInHourFormat,
    getDateInFormattedISOString,
    getWeekday } from '../utils/helper';

class WeekCalendar extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleEventClick = this.handleEventClick.bind(this);
        this.renderGridHeader = this.renderGridHeader.bind(this);
        this.renderWeekGrid = this.renderWeekGrid.bind(this);
    }

    /**
     * Function to handle click
     * Will get current click position and calculate the selected time in a 15 minutes slot for a given day
     * Will toggle event modal and set dummy modalData for the form
     * @param {Date} date
     * @param {event} event
     */
    handleClick(date, event) {
        let elementHeight = event.target.clientHeight,
            currentClickPosition = (event.pageY - event.target.getBoundingClientRect().top),
            selectedTimeSlot = (currentClickPosition/elementHeight * 1440),
            formattedSelectedTimeSlot = selectedTimeSlot > 1380 ? 1380 : (selectedTimeSlot - (selectedTimeSlot % 15)),
            selectedTimeSlotAfterHour = formattedSelectedTimeSlot + 60,
            modalData = {
                title: '',
                date,
                selectedTimeSlot: formattedSelectedTimeSlot,
                selectedTimeSlotAfterHour
            }
        this.props.setModalData(modalData);
        this.props.toggleModal();
    }

    /**
     * Function to get current event data and show update event modal
     * @param {*} e
     * @param {object} selectedEvent
     * @param {integer} eventIndex
     */
    handleEventClick(e, selectedEvent, eventIndex) {
        e.stopPropagation();
        let modalData = {
            title: selectedEvent.title,
            date: new Date(selectedEvent.selecteddate.split('/').reverse().join('-')),
            selectedTimeSlot: selectedEvent.to,
            selectedTimeSlotAfterHour: selectedEvent.from,
            eventIndex: eventIndex
        };
        this.props.setModalData(modalData);
        this.props.toggleModal();
    }

    // Function to render week grids
    renderWeekGrid(dates, events, hoursInADay) {
        return (
            <div className="calendar-col-container calendar-col-container-week">
                <div className="calendar-col calendar-col--secondary">
                    {
                        [...Array(hoursInADay).fill(1)].map((item, index) => {
                            return (
                                <div key={`${item + index}-hours`} className="calendar-col-time">
                                    <span className="calendar-col-time--primary">{getTimeIn24HoursFormat(item + index)}</span>
                                </div>
                            )
                        })
                    }
                </div>
                {
                    dates.dates.map((date, dateIndex) => {
                        return (
                            <div key={`dates-${dateIndex}`} className="calendar-col calendar-col--primary">
                                <div className="cursor-pointer"
                                    onClick={this.handleClick.bind(this, date)}
                                    style={{position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        bottom: 0,
                                        zIndex: 10,
                                        right: 0}}>
                                    {
                                        events && events[getDateInFormattedISOString(date)]
                                        ? (events[getDateInFormattedISOString(date)].map((currentEvent, currentEventIndex) => {
                                            return (<div className="event-pill"
                                                onClick={(e) => this.handleEventClick(e, currentEvent, currentEventIndex)}
                                                style={{ top: currentEvent.to + 'px'}}
                                                key={`week-events-${currentEventIndex}`}>
                                                    <div className="event-pill-text-container">
                                                        <p className="event-pill-text event-pill-text--primary">{currentEvent.title}</p>
                                                        <p className="event-pill-text event-pill-text--secondary">
                                                            {getMinutesInHourFormat(currentEvent.to).hours}:{getMinutesInHourFormat(currentEvent.to).minutes}
                                                            -
                                                            {getMinutesInHourFormat(currentEvent.from).hours}:{getMinutesInHourFormat(currentEvent.from).minutes}
                                                        </p>
                                                    </div>
                                                </div>)}))
                                        : (null)
                                    }
                                </div>
                                {
                                     [...Array(hoursInADay).fill(1)].map((item) => {
                                        return (
                                            <div className="calendar-col-item" key={Math.random() + item}></div>
                                        )
                                    }) // Creating faux boxes
                                }
                            </div>
                        )
                    })
                }
            </div>
        )
    }

    renderGridHeader(dates, today) {
        return (
            <div className="calendar-col-container calendar-col-container--header">
                <div className="calendar-col calendar-col--secondary"></div>
                {
                    dates.map((date, dateIndex) => {
                        return (
                            <div key={`date-header${dateIndex}`} className="calendar-col">
                                <div className={`calendar-col-header ${date.toLocaleString() === today.toLocaleString() ? 'calendar-col-header--active' : ''}`}>
                                    <p className="calendar-col-header-weekday">{getWeekday(date)}</p>
                                    <p className="calendar-col-header-date">{date.getDate()}</p>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )
    }

    render() {
        const { calendar } = this.props,
            { dates, availableEvents, today, hoursInADay } = calendar;
        return (
            <div className="calendar-container">
                { this.renderGridHeader(dates.dates, today) }
                { this.renderWeekGrid(dates, availableEvents, hoursInADay) }
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        calendar: state.calendar
    }
}

export default connect(mapStateToProps)(WeekCalendar);