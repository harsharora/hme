import React, { Component } from 'react';
import { connect } from 'react-redux';
import TopBar from '../components/TopBar';
import WeekCalendar from './WeekCalendar.js';
import EventModal from '../components/EventModal';
import { addOrUpdateEvent,
    deleteEvent } from '../actions/calendar';

import '../App.scss';

class CalendarApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showEventModal: false,
            modalData: {}
        }
        this.toggleEventModal = this.toggleEventModal.bind(this);
        this.saveEvent = this.saveEvent.bind(this);
        this.deleteEvent = this.deleteEvent.bind(this);
        this.setModalData = this.setModalData.bind(this);
    }

    toggleEventModal() {
        this.setState({
            showEventModal: !this.state.showEventModal
        })
    }

    setModalData(modalData) {
        this.setState({
            modalData
        })
    }

    saveEvent(e) {
        e.preventDefault();
        let formData = new FormData(e.target),
            eventObj = {};
        for (var pair of formData.entries()) {
            eventObj[pair[0]] = pair[1];
        }
        this.props.addOrUpdateEvent(eventObj);
        this.toggleEventModal();
    }

    deleteEvent(e) {
        e.preventDefault();
        let formData = new FormData(document.querySelector('#event-form')),
            eventObj = {};
        for (var pair of formData.entries()) {
            eventObj[pair[0]] = pair[1];
        }
        this.props.deleteEvent(eventObj);
        this.toggleEventModal();
    }

    render() {
        return (
            <div>
                <TopBar />
                <WeekCalendar
                    setModalData={this.setModalData}
                    toggleModal={this.toggleEventModal} />
                {
                    this.state.showEventModal
                    ? (<EventModal
                        toggleModal={this.toggleEventModal}
                        saveEvent={this.saveEvent}
                        deleteEvent={this.deleteEvent}
                        modalData={this.state.modalData} />)
                    : (null)
                }
            </div>
        )
    }
}

export default connect(null, {
    addOrUpdateEvent,
    deleteEvent
})(CalendarApp);