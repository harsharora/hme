import React, { Component } from 'react';
import CalendarApp from './containers/CalendarApp';
import { Provider } from 'react-redux';
import store from './store/index';
class App extends Component {
  render() {
    return (<Provider store={store}>
      <CalendarApp />
    </Provider>);
  }
}

export default App;
