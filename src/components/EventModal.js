import React, { Component } from 'react';
import { getMinutesInHourFormat } from '../utils/helper';

class EventModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.modalData ? this.props.modalData.title : ''
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleKeyUp = this.handleKeyUp.bind(this);
    }

    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleKeyUp(e) {
        if (e.key === 'Escape') {
            this.props.toggleModal();
        }
    }

    render() {
        const { modalData } = this.props,
            dateToShow = modalData.date.toUTCString().split(modalData.date.getFullYear())[0],
            selectedTimeSlot = modalData.selectedTimeSlot,
            selectedTimeSlotAfterHour = modalData.selectedTimeSlotAfterHour,
            formattedSelectedTimeslot = getMinutesInHourFormat(selectedTimeSlot),
            formattedSelectedTimeSlotAfterHour = getMinutesInHourFormat(selectedTimeSlotAfterHour),
            dateInLocalString = modalData.date.toISOString().split('T')[0],
            saveBtnText = typeof modalData.eventIndex !== 'undefined' ? 'Update' : 'Save'; // To see if a new event or selected event
        return (
            <div className="event-modal-container">
                <div className="event-modal">
                    <form id="event-form" onSubmit={this.props.saveEvent}>
                        <div>
                            <input name="title"
                                value={this.state.title}
                                className="event-title-input"
                                autoFocus={true}
                                onChange={this.handleInputChange}
                                onKeyUp={this.handleKeyUp}
                                placeholder={modalData && modalData.title ? modalData.title : 'Add event title'} />
                        </div>
                        <div className="event-time-input-container">
                            <p className="event-time-input-text">
                                {
                                    typeof modalData.eventIndex !== 'undefined'
                                    ? (<input type="hidden" value={modalData.eventIndex} name="eventindex" />)
                                    : (null)
                                }
                                <input name="selecteddate" value={dateInLocalString} type="hidden" />
                                <input name="to" value={selectedTimeSlot} type="hidden" />
                                <input name="from" value={selectedTimeSlotAfterHour} type="hidden" />
                                <span>{dateToShow} {formattedSelectedTimeslot.hours}:{formattedSelectedTimeslot.minutes}</span>
                                <span>-</span>
                                <span>{formattedSelectedTimeSlotAfterHour.hours}:{formattedSelectedTimeSlotAfterHour.minutes} {dateToShow}</span>
                            </p>
                        </div>
                        <div className="event-modal-footer">
                            {
                                typeof modalData.eventIndex !== 'undefined'
                                ? (<button type="button" className="event-modal-btn event-modal-btn--danger cursor-pointer" onClick={this.props.deleteEvent}>Remove Event</button>)
                                : (<div></div>)
                            }
                            <div>
                                <button type="button" onClick={this.props.toggleModal} className="event-modal-btn cursor-pointer">Cancel</button>
                                {
                                    selectedTimeSlot && selectedTimeSlotAfterHour
                                    ? (<button type="submit" className={`event-modal-btn event-modal-btn--primary cursor-pointer ${this.state.title.trim() ? '' : 'event-modal-btn--disabled'}`}
                                        disabled={this.state.title.trim() ? false : true}>{saveBtnText}</button>)
                                    : (null)
                                }
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default EventModal;