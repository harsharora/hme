import React, { Component } from 'react';
import { connect } from 'react-redux';
import { goToNextWeek,
    goToPrevWeek,
    goToToday } from '../actions/calendar';
import { getMonthToRender } from '../utils/helper';

class TopBar extends Component {
    render() {
        const { goToNextWeek,
            goToPrevWeek,
            goToToday,
            calendar } = this.props;
        const { dates } = calendar,
            firstWeekDay = dates.dates[0],
            lastWeekDay = dates.dates[dates.dates.length - 1],
            weekToShow = firstWeekDay && lastWeekDay
                ? firstWeekDay.getMonth() === lastWeekDay.getMonth()
                ? (<div className="calendar-week-text">
                    <p className="calendar-week-text--primary">{getMonthToRender(firstWeekDay, 'long')}</p>
                    <p>{firstWeekDay.getFullYear()}</p>
                </div>)
                : (<div className="calendar-week-text">
                    <p>
                        <span className="calendar-week-text--primary">{getMonthToRender(firstWeekDay, 'short')}</span>
                        <span>{firstWeekDay.getFullYear()}</span>
                    </p>
                    <p>
                        <span className="calendar-week-text--divider">-</span>
                        <span className="calendar-week-text--primary">{getMonthToRender(lastWeekDay, 'short')}</span>
                        <span>{lastWeekDay.getFullYear()}</span>
                    </p>
                </div>)
                : '';
        return (
            <div className="calendar-header">
                {weekToShow}
                <div className="calendar-action-container">
                    <button className="calendar-action-btn cursor-pointer" onClick={goToPrevWeek}>
                        <span className="calendar-action-btn-text--secondary">←</span>
                    </button>
                    <button className="calendar-action-btn cursor-pointer" onClick={goToToday}>
                        <span className="calendar-action-btn-text">Today</span>
                    </button>
                    <button className="calendar-action-btn cursor-pointer" onClick={goToNextWeek}>
                        <span className="calendar-action-btn-text--secondary">→</span>
                    </button>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        calendar: state.calendar
    }
}

export default connect(mapStateToProps, { goToNextWeek,
    goToPrevWeek,
    goToToday })(TopBar);