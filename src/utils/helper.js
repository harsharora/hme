// Function to get time in 24 hours format
// will return 00:00-23:59
export const getTimeIn24HoursFormat = (time) => {
    return (time === 24
        ? ''
        : (time < 10 ? '0' + time : time) + ' : 00');
}

/**
 * Function to get an object dates with week start date adn array
 * @param {Date object} date
 */
export const getWeekDates = (date) => {
    let currentDay = date.getDay(),
        firstWeekday = new Date(date.valueOf()),
        firstWeekdayFormatted = new Date(firstWeekday.setDate(firstWeekday.getDate() - currentDay)),
        dates = [firstWeekdayFormatted];
    for (let i = 1; i <= 6; i++) {
        let nextDate = new Date(firstWeekdayFormatted);
        nextDate.setDate(nextDate.getDate() + i);
        dates.push(nextDate);
    }
    return {
        dates
    };
}

// Function to get an object of minutes in hour / minutes format
export const getMinutesInHourFormat = (mins) => {
    let hours = Math.floor(mins/60),
        minutes = (mins%60);
    return {
        hours : hours < 10 ? '0' + hours : hours + '',
        minutes : minutes === 0 ? '00' : minutes + ''
    }
}

// To get date in a formatted string (YYYY-MM-DD)
export const getDateInFormattedISOString = (date) => date.toISOString().split('T')[0];

// To get weekday from a given date
export const getWeekday = (date) => new Intl.DateTimeFormat('en-US', {weekday: 'short'}).format(date);

// To get month from a given date - take options to show short or long (January/Jan)
export const getMonthToRender = (date, monthType) => date.toLocaleString('en-us', { month: monthType })